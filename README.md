Success Installation
====================

    cd /var/www/
    git clone https://TobiasMue91@bitbucket.org/TobiasMue91/success.git
    cd success
    sudo composer install
    HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
    app/console doctrine:database:create
    app/console doctrine:schema:create
    success:generate:categories