<?php
namespace Success\Bundle\CommandBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Success\Bundle\DomainBundle\Entity\Category;

class GenerateCategoriesCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('success:generate:categories')
            ->setDescription('Generate categories from your categories.yml!')
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'The path to the yaml file',
                realpath(__DIR__ .'/../').'/Resources/lists/categories.yml')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('path');
        if(!file_exists($path)){
            throw new \InvalidArgumentException(sprintf('The supplied file %s does not exist.', $path));
        }

        $categories = Yaml::parse(file_get_contents($path));
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $i = 0;
        foreach($categories as $categoryName => $options) {
            $i++;
            $newCategory = new Category();
            $newCategory->setName($categoryName);
            $newCategory->setPosition($i);
            $entityManager->persist($newCategory);
            $output->writeln("Category $categoryName created!");
        }
        $entityManager->flush();
    }
}