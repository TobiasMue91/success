<?php
namespace Success\Bundle\ServiceBundle\Services;

class ConfigManager
{
    /** @var array */
    private $commentableClasses;
    /** @var array */
    private $votableClasses;

    function __construct(array $commentableClasses, array $votableClasses)
    {
        $this->commentableClasses = $commentableClasses;
        $this->votableClasses = $votableClasses;
    }

    /**
     * @return array
     */
    public function getCommentableClasses()
    {
        return $this->commentableClasses;
    }

    /**
     * @return array
     */
    public function getVotableClasses()
    {
        return $this->votableClasses;
    }
}