<?php
namespace Success\Bundle\DomainBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait SlugableTrait
{
    /**
     * @var string
     */
    protected $slug;

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug === null ? null : strval($slug);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasSlug()
    {
        return $this->slug !== null;
    }
}