<?php
namespace Success\Bundle\DomainBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Success\Bundle\DomainBundle\Entity\User;

trait OwnerTrait
{
    /**
     * @var User
     */
    private $owner;

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return self
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }
}