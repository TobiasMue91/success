<?php
namespace Success\Bundle\DomainBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait VotableTrait
{
    /**
     * @var int
     */
    protected $score;

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return self
     */
    public function setScore($score)
    {
        $score = intval($score, 10);
        $this->score = $score < 0 ? 0 : $score;
        return $this;
    }

    /**
     * @param int $by
     * @return VotableTrait
     */
    public function increaseScore($by = 1){
        $this->setScore($this->getScore() + $by);
        return $this;
    }

    /**
     * @param int $by
     * @return VotableTrait
     */
    public function decreaseScore($by = 1){
        $this->setScore($this->getScore() - $by);
        return $this;
    }
}