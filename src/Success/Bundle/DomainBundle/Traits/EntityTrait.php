<?php
namespace Success\Bundle\DomainBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait EntityTrait
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setID($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasID()
    {
        return $this->id !== null && is_int($this->id) && $this->id > 0;
    }

    /**
     * @return void
     * @throws \LogicException
     */
    public function ensureValidID()
    {
        if (!$this->hasID()) {
            throw new \LogicException(sprintf('This entity %s has no id assigned', $this));
        }
    }
}