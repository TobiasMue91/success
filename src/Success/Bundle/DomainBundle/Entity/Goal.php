<?php

namespace Success\Bundle\DomainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Success\Bundle\CommentBundle\Entity\Commentable;
use Success\Bundle\CommentBundle\Traits\CommentableTrait;
use Success\Bundle\DomainBundle\Traits\EntityTrait;
use Success\Bundle\DomainBundle\Traits\OwnerTrait;
use Success\Bundle\DomainBundle\Traits\SlugableTrait;
use Success\Bundle\DomainBundle\Traits\TimestampableTrait;
use Success\Bundle\DomainBundle\Traits\VotableTrait;

class Goal implements Votable, Commentable, Slugable
{
    use TimestampableTrait;
    use EntityTrait;
    use VotableTrait;
    use SlugableTrait;
    use CommentableTrait;
    use OwnerTrait;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var Category */
    private $category;

    /** @var Step[]|ArrayCollection */
    private $steps;

    /** @var int */
    private $progress;

    function __construct()
    {
        $this->score = 0;
        $this->steps = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->progress = 0;
    }

    function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return 'goal';
    }

    /**
     * @param Step $step
     * @return $this
     */
    public function addStep(Step $step)
    {
        if($this->hasStep($step)) {
            throw new \InvalidArgumentException(sprintf('The supplied step %s has already been assigned to this category', $step));
        }
        $step->setGoal($this);
        $this->steps->add($step);
        return $this;
    }

    /**
     * @param Step $step
     * @return $this
     */
    public function removeStep(Step $step)
    {
        if(!$this->hasStep($step)) {
            throw new \InvalidArgumentException(sprintf('The supplied step %s has never been assigned to this category', $step));
        }
        $this->steps->removeElement($step);
        return $this;
    }

    /**
     * @param Step $step
     * @return bool
     */
    public function hasStep(Step $step)
    {
        return $this->steps->contains($step);
    }

    /**
     * @return ArrayCollection|Step[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     * @return self
     */
    public function setProgress($progress)
    {
        $this->progress = $progress > 100 ? 100 : $progress;
        return $this;
    }
}