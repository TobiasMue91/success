<?php

namespace Success\Bundle\DomainBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Model\MediaInterface;
use Success\Bundle\CommentBundle\Entity\Commentable;
use Success\Bundle\CommentBundle\Traits\CommentableTrait;
use Success\Bundle\DomainBundle\Traits\EntityTrait;
use Success\Bundle\DomainBundle\Traits\OwnerTrait;
use Success\Bundle\DomainBundle\Traits\TimestampableTrait;

class Step implements Commentable
{
    use TimestampableTrait;
    use EntityTrait;
    use CommentableTrait;
    use OwnerTrait;

    /** @var Goal */
    private $goal;

    /** @var string */
    private $description;

    /** @var Media */
    private $media;

    function __construct()
    {
        $this->score = 0;
        $this->comments = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return 'step';
    }

    /**
     * @return MediaInterface
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param MediaInterface $media
     * @return self
     */
    public function setMedia(MediaInterface $media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * @param Goal $goal
     * @return self
     */
    public function setGoal(Goal $goal)
    {
        $this->goal = $goal;
        return $this;
    }
}