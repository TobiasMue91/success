<?php
namespace Success\Bundle\DomainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Sortable\Sortable;
use Success\Bundle\DomainBundle\Traits\OwnerTrait;
use Success\Bundle\DomainBundle\Traits\SlugableTrait;
use Success\Bundle\DomainBundle\Traits\TimestampableTrait;
use Success\Bundle\DomainBundle\Traits\EntityTrait;
use Symfony\Component\Validator\Tests\Fixtures\EntityInterface;

class Category implements EntityInterface, Slugable, Sortable
{
    use TimestampableTrait;
    use EntityTrait;
    use OwnerTrait;
    use SlugableTrait;

    /** @var string */
    private $name;

    /** @var int */
    private $position;

    /** @var Goal[]|ArrayCollection */
    private $goals;

    function __construct()
    {
        $this->goals = new ArrayCollection();
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @param Goal $goal
     * @return $this
     */
    public function addGoal(Goal $goal)
    {
        if($this->hasGoal($goal)) {
            throw new \InvalidArgumentException(sprintf('The supplied goal %s has already been assigned to this category', $goal));
        }
        $goal->setCategory($this);
        $this->goals->add($goal);
        return $this;
    }

    /**
     * @param Goal $goal
     * @return $this
     */
    public function removeGoal(Goal $goal)
    {
        if(!$this->hasGoal($goal)) {
            throw new \InvalidArgumentException(sprintf('The supplied goal %s has never been assigned to this category', $goal));
        }
        $this->goals->removeElement($goal);
        return $this;
    }

    /**
     * @param Goal $goal
     * @return bool
     */
    public function hasGoal(Goal $goal)
    {
        return $this->goals->contains($goal);
    }

    /**
     * @return ArrayCollection|Goal[]
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @return bool
     */
    public function hasSlug()
    {
        return $this->slug !== null;
    }
}