<?php
namespace Success\Bundle\DomainBundle\Entity;

interface Identifiable extends EntityInterface
{
    /**
     * Is used to identify|reference the entity in http requests.
     *
     * @return string
     */
    public function getIdentifier();
}