<?php

namespace Success\Bundle\DomainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Success\Bundle\DomainBundle\Traits\SlugableTrait;
use Success\Bundle\DomainBundle\Traits\TimestampableTrait;

class User extends BaseUser implements Slugable
{
    use SlugableTrait;
    use TimestampableTrait;

    const FACTOR = 1.0;

    /** @var Goal[]|ArrayCollection */
    private $goals;

    /** @var int */
    private $rank;

    public function __construct()
    {
        parent::__construct();
        $this->goals = new ArrayCollection();
        $this->rank = 0;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return ArrayCollection|Goal[]
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param Goal $goal
     * @return self
     */
    public function addGoal(Goal $goal)
    {
        $this->goals->add($goal);
        return $this;
    }

    public function hasGoal(Goal $goal)
    {
        return $this->goals->contains($goal);
    }

    /**
     * @param Goal $goal
     *
     * @return self
     * @throws \InvalidArgumentException
     */
    public function removeGoal(Goal $goal)
    {
        if (!$this->hasGoal($goal)) {
            throw new \InvalidArgumentException(sprintf('The supplied goal %s has never been assigned to this user', $goal));
        }

        $this->goals->removeElement($goal);
        return $this;
    }

    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param $by
     * @return self
     */
    public function increaseRank($by)
    {
        $this->rank += $by;
        return $this;
    }

    /**
     * @param $by
     * @return self
     */
    public function decreaseRank($by)
    {
        $this->rank -= $by;
        return $this;
    }

    /**
     * @return int
     */
    public function getPower()
    {
        return intval($this->rank * self::FACTOR, 10);
    }
}