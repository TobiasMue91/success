<?php
namespace Success\Bundle\DomainBundle\Entity;

interface EntityInterface
{
    /**
     * @return int
     */
    public function getID();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setID($id);

    /**
     * @return bool
     */
    public function hasID();

    /**
     * @return void
     * @throws \LogicException
     */
    public function ensureValidID();
}