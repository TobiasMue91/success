<?php

namespace Success\Bundle\DomainBundle\Entity;

interface Votable extends Identifiable
{
    /**
     * @param int $by
     * @return self
     */
    public function increaseScore($by = 1);

    /**
     * @param int $by
     * @return self
     */
    public function decreaseScore($by = 1);

    /**
     * @return int
     */
    public function getScore();
}