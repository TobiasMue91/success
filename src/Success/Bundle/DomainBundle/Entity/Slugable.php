<?php
namespace Success\Bundle\DomainBundle\Entity;

interface Slugable
{
    /**
     * @return string
     */
    public function getSlug();

    /**
     * @param string $slug
     * @return Slugable
     */
    public function setSlug($slug);

    /**
     * @return bool
     */
    public function hasSlug();
}