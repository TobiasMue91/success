<?php

namespace Success\Bundle\CommentBundle\Controller;
use Success\Bundle\CommentBundle\Entity\Comment;
use Success\Bundle\CommentBundle\Entity\Commentable;
use Success\Bundle\DomainBundle\Entity\Goal;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    /**
     * @param Request $request
     * @param Goal|Comment $target
     * @param int $id
     * @param string $type
     * @return JsonResponse|Response
     */
    public function newAction(Request $request, $target = null, $id = 0, $type = '')
    {
        $user       = $this->getUser();
        $comment    = new Comment();
        $comment->setOwner($user);

        if (!$target instanceof Commentable && $id !== 0) {
            $target = $this->getTarget($id, $type);
        }

        $actionURL  = $this->generateUrl('success_comment_new', ['id' => $target->getID(), 'type' => $target->getIdentifier()]);
        $form       = $this->createForm('comment', $comment, ['action' => $actionURL]);

        return $this->renderForm($request, $target, $form, $comment);
    }

    /**
     * @param Request $request
     * @param Commentable $target
     * @param FormInterface $form
     * @param Comment $comment
     * @return Response
     */
    protected function renderForm(Request $request, Commentable $target, FormInterface $form, Comment $comment)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!$this->isGranted('ROLE_USER')) {
                    return new Response('Missing Permission!', Response::HTTP_FORBIDDEN);
                }
                $comment->setTarget($target);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();
                return $this->render('@SuccessComment/Comment/single.html.twig', ['comment' => $comment]);
            } else {
                // TODO: Validation
                return new Response('Validierungs-Fehler!', Response::HTTP_BAD_REQUEST);
            }
        }
        return $this->render('@SuccessComment/Comment/new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param $id
     * @param $type
     * @return Commentable
     */
    private function getTarget($id, $type)
    {
        $class = $this->get('success_service.config.manager')->getCommentableClasses()[$type];
        return $this->getDoctrine()->getRepository($class)->find($id);
    }
}