<?php
namespace Success\Bundle\CommentBundle\Traits;

use Success\Bundle\CommentBundle\Entity\Comment;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

trait CommentableTrait
{
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $totalComments = 0;

    /**
     * @var ArrayCollection|Comment[]
     */
    protected $comments;

    /**
     * @return int
     */
    public function getTotalComments()
    {
        return $this->totalComments;
    }

    /**
     * @param int $total
     * @return self
     */
    public function setTotalComments($total)
    {
        $total = intval($total, 10);
        $this->totalComments = $total < 0 ? 0 : $total;
        return $this;
    }

    /**
     * @return self
     */
    public function incTotalComments(){
        $this->setTotalComments($this->getTotalComments() + 1);
        return $this;
    }

    /**
     * @return self
     */
    public function decTotalComments(){
        $this->setTotalComments($this->getTotalComments() - 1);
        return $this;
    }

    /**
     * @param Comment $comment
     * @return self
     */
    public function addComment(Comment $comment){
        if($this->hasComment($comment)) {
            throw new \InvalidArgumentException(sprintf('The supplied comment %s has already been assigned to this object', $comment));
        }
        $this->comments->add($comment);
        return $this;
    }

    /**
     * @param Comment $comment
     * @return self
     */
    public function removeComment(Comment $comment){
        if(!$this->hasComment($comment)) {
            throw new \InvalidArgumentException(sprintf('The supplied comment %s has never been assigned to this object', $comment));
        }
        $this->comments->removeElement($comment);
        return $this;
    }

    /**
     * @return bool
     */
    public function hasAnyComment(){
        return $this->comments->count() > 0;
    }

    /**
     * @param Comment $comment
     * @return bool
     */
    public function hasComment(Comment $comment){
        return $this->comments->contains($comment);
    }

    /**
     * @return Comment[]|ArrayCollection
     */
    public function getComments(){
        return $this->comments;
    }
}