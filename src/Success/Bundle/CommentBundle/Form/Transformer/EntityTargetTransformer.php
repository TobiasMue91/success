<?php
namespace Success\Bundle\CommentBundle\Form\Transformer;

use Doctrine\ORM\EntityManager;
use Success\Bundle\CommentBundle\Entity\Commentable;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EntityTargetTransformer extends AbstractEntityTransformer
{
    /** @var string[] */
    protected $shortNameToClass;
    /** @var string[] */
    protected $classToShortName;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->createClassMap();
    }

    protected function createClassMap()
    {
        $this->shortNameToClass = [];

        /** @var \Doctrine\Common\Persistence\Mapping\ClassMetadata[] $allMetadata */
        $allMetadata = $this->entityManager->getMetadataFactory()->getAllMetadata();

        foreach ($allMetadata as $metadata) {
            $shortName = strtolower($metadata->getReflectionClass()->getShortName());
            $counter = 1;

            $finalShortName = $shortName;
            while (array_key_exists($finalShortName, $this->shortNameToClass)) {
                $finalShortName = $shortName . $counter;
                $counter++;
            }

            $this->shortNameToClass[$finalShortName] = $metadata->getName();
        }

        $this->classToShortName = array_flip($this->shortNameToClass);
    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * @param object $value The value in the original representation
     * @return string The value in the transformed representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function transform($value)
    {
        if (null === $value) {
            return "";
        }

        $className = $this->ensureIsEntity($value);
        return $this->classNameToShortName($className) . ':' . $this->getIdentifierValue($value, $className);
    }

    /**
     * @param string $className
     * @return string
     */
    protected function classNameToShortName($className)
    {
        return array_key_exists($className, $this->classToShortName) ? $this->classToShortName[$className] : null;
    }

    /**
     * @param mixed $value The value in the transformed representation
     * @return object The value in the original representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $split = explode(':', $value);
        if (count($split) !== 2) {
            throw new TransformationFailedException();
        }

        $shortClassName = $split[0];
        $fullClassName = $this->shortNameToClass($shortClassName);
        if ($fullClassName === null) {
            throw new TransformationFailedException();
        }

        $id = $split[1];
        if (!is_numeric($id)) {
            throw new TransformationFailedException();
        }

        $id = intval($id, 10);
        if ($id < 1) {
            throw new TransformationFailedException();
        }

        $entity = $this->entityManager->getRepository($fullClassName)->find($id);

        if (null === $entity) {
            throw new TransformationFailedException();
        }

        return $entity;
    }

    /**
     * @param string $shortName
     * @return string
     */
    protected function shortNameToClass($shortName)
    {
        return array_key_exists($shortName, $this->shortNameToClass) ? $this->shortNameToClass[$shortName] : null;
    }
}