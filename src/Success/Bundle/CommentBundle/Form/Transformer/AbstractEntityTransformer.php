<?php
namespace Success\Bundle\CommentBundle\Form\Transformer;

use Doctrine\Common\Persistence\Proxy;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

abstract class AbstractEntityTransformer implements DataTransformerInterface
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * @param object $value
     * @param string $className
     * @return int
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    protected function getIdentifierValue($value, $className)
    {
        $metadata = $this->entityManager->getClassMetadata($className);
        $identifierProperty = $metadata->getReflectionClass()->getProperty($metadata->getSingleIdentifierFieldName());
        $identifierProperty->setAccessible(true);
        $identifier = $identifierProperty->getValue($value);
        $identifierProperty->setAccessible(false);
        return $identifier;
    }

    /**
     * @param $value
     * @return string
     */
    protected function ensureIsEntity($value)
    {
        if (!is_object($value)) {
            throw new TransformationFailedException('Must be an object');
        }

        $className = $class = ($value instanceof Proxy)
            ? get_parent_class($value)
            : get_class($value);

        if (!$this->entityManager->getMetadataFactory()->hasMetadataFor($className)) {
            throw new TransformationFailedException('Must be an entity managed by doctrine');
        }
        return $className;
    }
} 