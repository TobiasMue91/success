<?php
namespace Success\Bundle\CommentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Success\Bundle\DomainBundle\Entity\Identifiable;

interface Commentable extends Identifiable{

    /**
     * @return self
     */
    public function incTotalComments();

    /**
     * @return self
     */
    public function decTotalComments();

    /**
     * @param int $total
     * @return self
     */
    public function setTotalComments($total);

    /**
     * @return int
     */
    public function getTotalComments();

    /**
     * @param Comment $comment
     * @return self
     */
    public function addComment(Comment $comment);

    /**
     * @param Comment $comment
     * @return self
     */
    public function removeComment(Comment $comment);

    /**
     * @return bool
     */
    public function hasAnyComment();

    /**
     * @param Comment $comment
     * @return bool
     */
    public function hasComment(Comment $comment);

    /**
     * @return Comment[]|ArrayCollection
     */
    public function getComments();

}