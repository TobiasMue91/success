<?php
namespace Success\Bundle\CommentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Success\Bundle\CommentBundle\Traits\CommentableTrait;
use Success\Bundle\DomainBundle\Entity\Votable;
use Success\Bundle\DomainBundle\Traits\EntityTrait;
use Success\Bundle\DomainBundle\Traits\OwnerTrait;
use Success\Bundle\DomainBundle\Traits\TimestampableTrait;
use Success\Bundle\DomainBundle\Traits\VotableTrait;

class Comment implements Votable, Commentable
{
    use EntityTrait;
    use VotableTrait;
    use OwnerTrait;
    use TimestampableTrait;
    use CommentableTrait;

    /** @var string */
    private $text;

    /** @var Commentable */
    private $target;

    function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->score = 0;
    }

    /**
     * Is used to identify|reference the entity in http requests.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return 'comment';
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return Commentable
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param Commentable $target
     * @return Comment
     */
    public function setTarget(Commentable $target)
    {
        $this->target = $target;
        $target->addComment($this);
        return $this;
    }
}