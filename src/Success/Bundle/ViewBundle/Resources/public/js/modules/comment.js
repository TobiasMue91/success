window.success.comment = (function ($, window, document, undefined) {
    var module,
        core = window.success.core,
        $commentList,
        $container;

    var callbacks = {
        submitMain: function(event) {
            event.preventDefault();
            var $this = $(this);
            core.form.sendFormViaAjax($this)
                .done(function(data){
                    $container.find('.comment-list').prepend(data);
                    $this.find('textarea').val('');
                });
        },

        submit: function(event) {
            event.preventDefault();
            var $this = $(this);
            core.form.sendFormViaAjax($this)
                .done(function(data){
                    $this.replaceWith(data);
                });
        },

        loadCommentForm: function(event) {
            event.preventDefault();
            var $this = $(this),
                link = $this.attr('href');

            $.get(link).done(function(data){
                $this.replaceWith(data);
            });
        }
    };

    module = {
        init: function () {
            $container = $('.comment-container');
            $commentList = $container.find('.comment-list');

            $container.on('submit', '.main-comment form', callbacks.submitMain);
            $container.on('click', '.add-comment', callbacks.loadCommentForm);
            $commentList.on('submit', 'form', callbacks.submit);
        }
    };

    return module;
}(jQuery, window, document));