(function ($, window, document, undefined) {
    window.success = {};
    window.success.core = {};

    $(function () {
        window.success.core.loader.init();
    });

})(jQuery, window, document);