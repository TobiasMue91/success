window.success.core.form = (function ($, window, document, undefined) {

    var ajaxHelpers = {
        sendFormViaAjax: function ($form) {
            var serializedForm = $form.serializeArray();
            $form.find('[type="submit"]').addClass('disabled');
            $form.find('fieldset').addClass('disabled');

            return $.ajax({
                'type': $form.attr('method') || 'POST',
                'url': $form.attr('action'),
                'data': serializedForm
            });
        }
    };

    var uiHelpers = {
        applyErrorsToForm: function ($form, errors, options) {
            uiHelpers.removeErrorsFromForm($form, options);

            for (var index in errors) {
                var error = errors[index],
                    compound = error.compound,
                    inputName = error.name,
                    wrapID = error.id,
                    errorMessage = error.message,
                    $input = !compound ? $form.find('[name="' + inputName + '"]') : $('#' + wrapID);

                if($input.length > 0){
                    $input.closest('.form-group').addClass('has-error');
                    $input.after($('<p class="help-block js-block-core-form"></p>').html(errorMessage));
                }

                callback.dispatch(options, 'onError', [error]);
            }
        },

        removeErrorsFromForm: function($form, options){
            callback.dispatch(options, 'onRemoveErrors');
            $form.find('.form-group').removeClass('has-error');
            $form.find('.js-block-core-form').remove();
        },

        preventSpamSubmit: function (event) {

            var $this = $(this),
                lastTimeSubmitted = $this.data('last-submitted'),
                threshold = $this.data('threshold') || $(event.delegateTarget).data('threshold'),
                now = new Date().getTime();

            if (!lastTimeSubmitted) {
                $this.data('last-submitted', now);
                return;
            }

            if (lastTimeSubmitted + threshold > now) {
                event.preventDefault();
                event.stopImmediatePropagation();
                return;
            }

            $this.data('last-submitted', now);
        }
    };

    var callback = {
        dispatch: function(options, event, args){
            if(!options[event] || !$.isFunction(options[event])){
                return;
            }

            options[event].apply(this, args || []);

        }
    };

    return {
        /**
         *
         * @param $form
         * @param options
         * @returns {*}
         */
        sendFormViaAjax: function ($form, options) {
            var result = $.Deferred();

            options = options || {};

            ajaxHelpers.sendFormViaAjax($form)
                .done(function (ajaxResult) {
                    uiHelpers.removeErrorsFromForm($form, options);
                    result.resolve(ajaxResult);
                }).fail(function (jXhr) {
                    if (jXhr.status === 400 && jXhr.responseJSON && jXhr.responseJSON.errors) {
                        uiHelpers.applyErrorsToForm($form, jXhr.responseJSON.errors, options);
                        result.reject(jXhr.responseJSON.errors);
                    } else {
                        uiHelpers.removeErrorsFromForm($form, options);
                        result.reject(jXhr);
                    }
                }).always(function () {
                    $form.find('[type="submit"]').removeClass('disabled')
                    $form.find('fieldset').removeClass('disabled');
                });

            return result;
        },

        antiSpam: function ($form, thresholdInSecs, selector) {

            if ($form.data('anti_spam')) {
                return;
            }

            $form.data({
                    'anti_spam': true,
                    'threshold': thresholdInSecs * 1000
                }
            );

            if(selector){
                $form.on('submit.anti_spam', selector, uiHelpers.preventSpamSubmit);
            }else{
                $form.on('submit.anti_spam', uiHelpers.preventSpamSubmit);
            }
        },

        destroyAntiSpam: function ($form) {
            $form.off('submit.anti_spam');
            $form.removeData(['anti_spam', 'threshold', 'last-submitted']);
        }
    }
}(jQuery, window, document));