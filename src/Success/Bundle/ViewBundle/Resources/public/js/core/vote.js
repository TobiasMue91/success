window.success.core.vote = (function ($, window, document, undefined) {
    var module,
        core = window.success.core,
        $container;

    var callbacks = {
        vote: function(event) {
            event.preventDefault();
            var $this = $(this),
                link = $this.attr('href');

            $.get(link).done(function(data){
                $this.parent().siblings('.score').empty().append(data);
                $this.removeClass('vote');
                $this.addClass('disabled');
                $this.attr('href', '#');
            });
        }
    };

    module = {
        init: function () {
            $container = $('body');
            $container.on('click', '.vote', callbacks.vote);
        }
    };

    return module;
}(jQuery, window, document));