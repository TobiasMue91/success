window.success.core.loader = (function($, window, document, undefined){
    var core = window.success.core,
        promises = {};

    var loader = {

        init: function(){
            var $namesHolder = $('#js-modules'),
                moduleNames = loader.modules = $namesHolder.length ? $namesHolder.val().split(' ') : [];

            this.initCoreModules();
            this.loadModules(moduleNames);
        },

        initModule: function(module, deferredKey){
            if (!module.init || !$.isFunction(module.init)) {
                return;
            }

            module.init.call(success);
            if(deferredKey){
                promises[deferredKey].resolve();
            }
        },

        destroyModule: function(module){
            if (!module.destroy || !$.isFunction(module.destroy)) {
                return;
            }

            module.destroy.call(success);
        },

        load: function (moduleName) {
            if (!(moduleName in success)) {
                return;
            }

            this.initModule(success[moduleName]);
        },

        unload: function (moduleName) {
            if (!(moduleName in success)) {
                return;
            }
            this.destroyModule(success[moduleName]);
        },

        loadModules: function (moduleNames) {
            for (var index in moduleNames) {
                var moduleName = moduleNames[index];
                loader.load(moduleName);
            }
        },

        unloadModules: function (moduleNames) {
            for (var index in moduleNames) {
                var moduleName = moduleNames[index];
                loader.unload(moduleName);
            }
        },

        initCoreModules: function(){
            for (var coreModuleName in core) {
                if(coreModuleName === 'loader'){
                    continue;
                }

                promises['core.' + coreModuleName] = $.Deferred();
            }

            for (var coreModuleName in core) {
                if(coreModuleName === 'loader'){
                    continue;
                }

                this.initModule(core[coreModuleName], 'core.' + coreModuleName);
            }
        },

        onLoaded: function(modules){
            modules = $.isArray(modules) ? modules : [modules];
            if(modules.length === 1){
                return promises[modules].promise();
            }
        }
    };

    return loader;
}(jQuery, window, document));