<?php
namespace Success\Bundle\ViewBundle\Twig;

final class GlobalJsModules {
    public static $entries = [];

    /**
     * @param array $entires
     */
    public static function add(array $entires = []){
        self::$entries = array_unique(array_merge(self::$entries, array_values($entires)));
    }
} 