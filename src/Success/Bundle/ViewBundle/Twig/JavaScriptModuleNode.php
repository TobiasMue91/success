<?php
namespace Success\Bundle\ViewBundle\Twig;

use Twig_Node;

class JavaScriptModuleNode extends Twig_Node {

    public function __construct(array $data, $lineno = 0, $tag = null)
    {
        parent::__construct(array(), array('data' => $data), $lineno, $tag);
    }

    public function compile(\Twig_Compiler $compiler)
    {
        $compiler
            ->write('\Success\Bundle\ViewBundle\Twig\GlobalJsModules::add(')
            ->repr($this->getAttribute('data'))
            ->raw(");\n");
    }
}