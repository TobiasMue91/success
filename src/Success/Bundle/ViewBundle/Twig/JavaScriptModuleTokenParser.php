<?php
namespace Success\Bundle\ViewBundle\Twig;

use Twig_Error_Syntax;
use Twig_Node;
use Twig_Token;

class JavaScriptModuleTokenParser extends \Twig_TokenParser{

    /**
     * Parses a token and returns a node.
     *
     * @param Twig_Token $token A Twig_Token instance
     * @return Twig_Node A Twig_NodeInterface instance
     * @throws Twig_Error_Syntax
     */
    public function parse(Twig_Token $token)
    {
        $stream = $this->parser->getStream();

        $data = [];

        while (!$stream->test(\Twig_Token::BLOCK_END_TYPE)) {
            $data[] = $stream->expect(\Twig_Token::STRING_TYPE)->getValue();
        }

        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return new JavaScriptModuleNode($data, $token->getLine(), $this->getTag());
    }

    /**
     * Gets the tag name associated with this token parser.
     *
     * @return string The tag name
     */
    public function getTag()
    {
        return 'js_module';
    }
}