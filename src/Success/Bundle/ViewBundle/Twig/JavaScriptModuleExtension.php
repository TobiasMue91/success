<?php
namespace Success\Bundle\ViewBundle\Twig;

class JavaScriptModuleExtension extends \Twig_Extension
{
    public function getTokenParsers()
    {
        return [new JavaScriptModuleTokenParser()];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('js_modules', function () {
                return GlobalJsModules::$entries;
            })
        ];
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'success_js_module';
    }
}