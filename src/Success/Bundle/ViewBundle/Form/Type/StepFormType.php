<?php
namespace Success\Bundle\ViewBundle\Form\Type;

use Success\Bundle\DomainBundle\Entity\Step;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StepFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', 'textarea', [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'Beschreibung'
                ]
            ])
            ->add('media', 'sonata_media_type', [
                'provider' => 'sonata.media.provider.image',
                'context'  => 'default'
            ])
            ->add('submit', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'step';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Step::class,
            'action'        => '',
        ]);
    }
}