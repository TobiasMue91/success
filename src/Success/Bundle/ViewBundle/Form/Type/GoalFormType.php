<?php
namespace Success\Bundle\ViewBundle\Form\Type;

use Success\Bundle\DomainBundle\Entity\Category;
use Success\Bundle\DomainBundle\Entity\Goal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoalFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'Titel des Ziels'
                ]
            ])
            ->add('description', 'textarea', [
                'label' => false,
                'attr'  => [
                    'placeholder' => 'Beschreibung'
                ]
            ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'goal';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Goal::class,
            'action'        => '',
        ]);
    }
}