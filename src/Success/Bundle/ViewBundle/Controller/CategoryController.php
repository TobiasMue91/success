<?php

namespace Success\Bundle\ViewBundle\Controller;

use Success\Bundle\DomainBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends BaseController
{
    public function indexAction()
    {
        if(!$this->isGranted('ROLE_USER')) {
            return $this->forward('FOSUserBundle:Registration:register');
        }
        $categories = $this->getRepository(Category::class)->findAll();
        return $this->render('SuccessViewBundle:Category:index.html.twig', ['categories' => $categories]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        $category = new Category();

        $actionURL = $this->generateUrl('success_category_new');
        return $this->renderForm($request, $category, 'new', null, ['action' => $actionURL]);
    }
}
