<?php

namespace Success\Bundle\ViewBundle\Controller;

use Success\Bundle\DomainBundle\Entity\Category;
use Success\Bundle\DomainBundle\Entity\Goal;
use Success\Bundle\DomainBundle\Entity\Step;
use Success\Bundle\DomainBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class GoalController extends BaseController
{
    /**
     * @param Category $category
     * @return Response
     *
     * @ParamConverter("category", class="Success\Bundle\DomainBundle\Entity\Category")
     */
    public function indexAction(Category $category)
    {
        return $this->render('SuccessViewBundle:Goal:index.html.twig', ['category' => $category]);
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return JsonResponse
     *
     * @ParamConverter("category", class="Success\Bundle\DomainBundle\Entity\Category")
     */
    public function newAction(Request $request, Category $category)
    {
        $goal = new Goal();
        $goal->setCategory($category);

        $actionURL = $this->generateUrl('success_goal_new', ['slug' => $category->getSlug()]);
        return $this->renderForm($request, $goal, 'new', null, ['action' => $actionURL]);
    }

    /**
     * @param Request $request
     * @param Goal $goal
     * @return JsonResponse
     *
     * @ParamConverter("goal", class="Success\Bundle\DomainBundle\Entity\Goal")
     */
    public function editAction(Request $request, Goal $goal)
    {
        $actionURL = $this->generateUrl('success_goal_edit', ['slug' => $goal->getSlug(), 'category' => $goal->getCategory()->getSlug()]);
        return $this->renderForm($request, $goal, 'edit', null, ['action' => $actionURL]);
    }

    /**
     * @param Goal $goal
     * @return Response
     * @ParamConverter("goal", class="Success\Bundle\DomainBundle\Entity\Goal")
     */
    public function showAction(Goal $goal)
    {
        return $this->render('@SuccessView/Goal/single.html.twig', ['goal' => $goal]);
    }


    /**
     * @param Request $request
     * @param Goal $goal
     * @return JsonResponse
     * @ParamConverter("goal", class="Success\Bundle\DomainBundle\Entity\Goal")
     */
    public function newStepAction(Request $request, Goal $goal)
    {
        $step = new Step();
        $step->setGoal($goal);

        $actionURL = $this->generateUrl('success_step_new', ['slug' => $goal->getSlug()]);
        return $this->renderForm($request, $step, 'new', null, ['action' => $actionURL]);
    }
}
