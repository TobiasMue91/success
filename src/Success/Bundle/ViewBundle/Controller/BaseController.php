<?php

namespace Success\Bundle\ViewBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    /**
     * @param mixed $object
     * @return string
     */
    protected function getClassName($object)
    {
        return (new \ReflectionClass($object))->getShortName();
    }

    /**
     * @param Request $request
     * @param mixed $object
     * @param string $actionType
     * @param callable $callback
     * @param array $options
     * @param string $bundle
     * @return JsonResponse
     */
    protected function renderForm(Request $request, $object, $actionType, $callback = null, $options, $bundle = 'SuccessView')
    {
        $className      = $this->getClassName($object);
        $lowerClassName = strtolower($className);
        $form           = $this->createForm($lowerClassName, $object, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if(!$this->isGranted('ROLE_USER')) {
                    return new Response('Missing Permission!', Response::HTTP_FORBIDDEN);
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($object);
                $entityManager->flush();
                if($callback) {
                    return call_user_func($callback, $object);
                }
            }
            else {
                // TODO: Validation
                return new Response('Validierungs-Fehler!', Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->render('@'.$bundle.'/'.$className.'/'.$actionType.'.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string        $class
     * @param ObjectManager $objectManager
     * @return ObjectRepository
     */
    protected function getRepository($class, ObjectManager $objectManager = null)
    {
        if(!$objectManager) {
            $objectManager = $this->getObjectManager();
        }
        return $objectManager->getRepository($class);
    }
}
