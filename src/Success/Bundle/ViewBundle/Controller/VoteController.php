<?php

namespace Success\Bundle\ViewBundle\Controller;

use Success\Bundle\DomainBundle\Entity\Votable;
use Success\Bundle\DomainBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class VoteController extends BaseController
{
    /**
     * @param Votable $target
     * @param int $id
     * @param string $type
     * @return Response
     */
    public function upVoteAction(Votable $target = null, $id = 0, $type = '')
    {
        if(!$target instanceof Votable) {
            $target = $this->getTarget($id, $type);
        }
        return $this->vote($target, false);
    }

    /**
     * @param Votable $target
     * @param int $id
     * @param string $type
     * @return Response
     */
    public function downVoteAction(Votable $target = null, $id = 0, $type = '')
    {
        if(!$target instanceof Votable) {
            $target = $this->getTarget($id, $type);
        }
        return $this->vote($target, true);
    }

    /**
     * @param Votable $target
     * @param bool $down
     * @return Response
     */
    private function vote(Votable $target, $down = false)
    {
        if(!$this->isGranted('ROLE_USER')) {
            return new Response(sprintf('You are not permitted to vote for this %s!', $target->getIdentifier()), Response::HTTP_FORBIDDEN);
        }

        /** @var User $user */
        $user   = $this->getUser();
        $power  = $user->getPower();

        $down ? $target->decreaseScore($power) : $target->increaseScore($power);
        $this->getDoctrine()->getManager()->flush();
        return new Response($target->getScore(), Response::HTTP_OK);
    }

    /**
     * @param $id
     * @param $type
     * @return Votable
     */
    private function getTarget($id, $type)
    {
        $class = $this->get('success_service.config.manager')->getVotableClasses()[$type];
        return $this->getDoctrine()->getRepository($class)->find($id);
    }
}
